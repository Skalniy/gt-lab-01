#!/usr/bin/env ipython

import numpy as np
import pandas as pd


def get_counts(col, target):
    return np.array([(col == i).sum() for i in target])


if __name__ == "__main__":
    C = np.array(eval(input('Enter game matrix: as 2D list:\n')))

    u = np.ones(C.shape[0])
    C_inv = np.linalg.inv(C)
    x = np.dot(u, C_inv) / np.dot(np.dot(u, C_inv), u.T)
    y = np.dot(C_inv, u.T) / np.dot(np.dot(u, C_inv), u.T)
    v = 1 / np.dot(np.dot(u, C_inv), u.T)

    print('Analytical solution:')
    print('x:', x)
    print('y:', y)
    print('v:', v)

    x_strat_id, y_strat_id = np.random.randint(
        C.shape[0]), np.random.randint(C.shape[1])

    df = pd.DataFrame(
        [[
            x_strat_id,
            y_strat_id,
            *C[:, y_strat_id],
            *C[x_strat_id, :],
            C[:, y_strat_id].max(),
            C[x_strat_id, :].min(),
            C[:, y_strat_id].max() - C[x_strat_id, :].min()
        ]],
        columns=['A', 'B', 'x1', 'x2', 'x3',
                 'y1', 'y2', 'y3', 'high', 'low', 'e']
    )

    while df.loc[df.index[-1], 'e'] > 0.1:
        x_costs = df.loc[df.index[-1], 'x1':'x3']
        y_costs = df.loc[df.index[-1], 'y1':'y3']

        x_strat_id = x_costs.index.get_loc(
            (np.random.choice(x_costs.index[x_costs == x_costs.values.max()])))

        y_strat_id = y_costs.index.get_loc(
            (np.random.choice(y_costs.index[y_costs == y_costs.values.min()])))

        df = df.append(
            pd.DataFrame(
                [[
                    x_strat_id,
                    y_strat_id,
                    *(C[:, y_strat_id] + df.loc[df.index[-1], 'x1':'x3']),
                    *(C[x_strat_id, :] + df.loc[df.index[-1], 'y1':'y3'])
                ]],
                columns=['A', 'B', 'x1', 'x2', 'x3', 'y1', 'y2', 'y3'],
            ),
            ignore_index=True
        )

        df.loc[df.index[-1], 'high'] = df.loc[df.index[-1], 'x1':'x3'].max() / \
            df.shape[0]
        df.loc[df.index[-1], 'low'] = df.loc[df.index[-1], 'y1':'y3'].min() / \
            df.shape[0]
        df.loc[df.index[-1], 'e'] = df['high'].min() - df['low'].max()

    x, y = get_counts(df['A'], range(C.shape[0])), get_counts(
        df['B'], range(C.shape[1]))

    print(df)
    print()
    print('x:', x / df.shape[0])
    print('y:', y / df.shape[0])
    print('v:', np.average([df['high'].min(), df['low'].max()]))
