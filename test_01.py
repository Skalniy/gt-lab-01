import numpy as np

ndots = int(input('Количество точек: '))  # количество точек
R = float(input('Радиус обнаружения: '))  # радиус сферы вокруг точки
a = float(input('Длина ребра куба: '))  # длина ребра куба
games_count = int(input('Количество игр: '))


def gen_sphere(n):
    coords = np.array([[], []])
    s = np.array([])

    while coords.shape[1] != n:
        assert coords.shape[1] == s.shape[0]

        tmp_c = np.random.uniform(-1, 1, size=(2, n - coords.shape[1]))
        tmp_s = np.square(tmp_c[0]) + np.square(tmp_c[1])
        tmp_ok = tmp_s < 1

        coords = np.concatenate(
            [coords, np.compress(tmp_ok, tmp_c, axis=1)], axis=1)
        s = np.concatenate([s, tmp_s[tmp_ok]])

        z = 1 - 2*s

    m = np.sqrt((1 - np.square(z)) / s)

    x = coords[0] * m / 2
    y = coords[1] * m / 2
    z = z / 2

    return np.stack([x, y, z])


class CubeFace:
    def __init__(self, axis, const):
        '''
        Представление грани куба, центром которого является точка O (0, 0, 0).

        Parameters
        ==========
        const
            Смещение относительно точки О и размер грани

        axis
            Ось смещения относительно точки 0
        '''
        self.axis = axis
        self.const = const

    def cross(self, point):
        '''
        Возвращает точку пересечения плоскости и радиус-луча, проходящего от точки O до целевой,
        и коэффициент радиус-вектора
        '''
        from numpy import dot

        e = self.eq()
        t = -e[-1]/dot(e[:3], point.T)
        return point * t, t

    def dots(self):
        '''Возвращает три точки грани.'''
        from numpy import reshape

        return reshape(self.grid().T, (4, 3))[:3]

    def eq(self):
        '''
        Возвращает коэффициенты уравнения плоскости, в которой лежит грань.
        '''
        from numpy import append
        from numpy.linalg import solve

        return append(solve(self.dots(), [-1] * 3), 1)

    def grid(self):
        '''Возвращает 4 точки, которые задают грань.'''
        from numpy import array, full, meshgrid

        res = meshgrid(*[[-self.const, self.const]]*2)
        assert res[0].shape == res[1].shape

        res.insert(self.axis, full(res[0].shape, self.const))
        return array(res)

    def is_on_surf(self, point):
        '''Принадлежит ли точка грани.'''
        if point[self.axis] != self.const:
            return False

        for i in range(3):
            if i == self.axis:
                continue

            if (point[i] < -abs(self.const)) or (point[i] > abs(self.const)):
                return False

        return True


def create_cube(a):
    from itertools import product

    return [CubeFace(*tmp) for tmp in product(range(3), [-abs(a), abs(a)])]


def projection_to_surface(surf, points):
    '''Проекция всех точек на грань'''
    coords, param = np.apply_along_axis(
        surf.cross, 0, np.array([*points], dtype=np.longdouble))
    coords = np.stack(coords)
    on_surf = np.apply_along_axis(surf.is_on_surf, 0, coords.T)

    return coords[on_surf & (param > 0)]


def projection_to_cube(surfaces, points):
    '''Проекция всех точек на все грани'''
    return np.concatenate([projection_to_surface(surf, points) for surf in surfaces])


def gen_cube(n, cube):
    cube_points = projection_to_cube(cube, gen_sphere(n))
    while cube_points.shape[0] != n:
        cube_points = np.concatenate(
            [cube_points, projection_to_cube(cube, gen_sphere(n - cube_points.shape[0]))])

    return cube_points


def in_sphere(O, r, point):
    '''Принадлежит ли точка сфере'''
    return ((O - point) ** 2).sum() <= r ** 2


def get_rand_point(surfs):
    '''Возвращает случайную точку на поверхностях'''
    surf = np.random.choice(surfs)
    c = (np.random.random(size=2) * abs(surf.const)
         * 2 - abs(surf.const)).tolist()
    c.insert(surf.axis, surf.const)

    return np.array(c)


def game(n, R, a):
    '''
    Игра.

    Parameters
    ==========
    n
        Количество точек.
    R
        Радиус сферы обнаружения.
    a
        Длина ребра куба.

    Returns
    =======
    bool
        Обнаружена ли точка, размещенная вторым игроком.
    '''
    cube = create_cube(a / 2)
    cube_pts = gen_cube(n, cube)
    p = get_rand_point(cube)

    return np.apply_along_axis(lambda o: in_sphere(o, R, p), 0, cube_pts.T).any()


cnt = sum([game(ndots, R, a) for _ in range(games_count)])
print('Цена игры методом моделирования:', cnt / games_count)

s_cube = (a ** 2) * 6
s_sphere = ndots * np.pi * (R ** 2)
print('Цена игры методом площадей:', s_sphere / s_cube)
