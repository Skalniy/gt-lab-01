import numpy as np
import pandas as pd
import sympy as sym


def print_result(x, y, v):
    print('x={} y={} H={}'.format(*[str(round(_, 3)) for _ in [x, y, v]]))


def braun_robinson(C):
    def get_counts(col, target):
        return np.array([(col == i).sum() for i in target])

    x_strat_id, y_strat_id = np.random.randint(
        C.shape[0]), np.random.randint(C.shape[1])

    x_strat_names = ['x{}'.format(_) for _ in range(C.shape[0])]
    y_strat_names = ['y{}'.format(_) for _ in range(C.shape[1])]

    df = pd.DataFrame(
        [[
            x_strat_id,
            y_strat_id,
            *C[:, y_strat_id],
            *C[x_strat_id, :],
            C[:, y_strat_id].max(),
            C[x_strat_id, :].min(),
            C[:, y_strat_id].max() - C[x_strat_id, :].min()
        ]],
        columns=['A', 'B', *x_strat_names, *y_strat_names, 'high', 'low', 'e']
    )

    while df.loc[df.index[-1], 'e'] > 0.1:
        x_costs = df.loc[df.index[-1], x_strat_names[0]:x_strat_names[-1]]
        y_costs = df.loc[df.index[-1], y_strat_names[0]:y_strat_names[-1]]

        x_strat_id = x_costs.index.get_loc(
            (np.random.choice(x_costs.index[x_costs == x_costs.values.max()])))

        y_strat_id = y_costs.index.get_loc(
            (np.random.choice(y_costs.index[y_costs == y_costs.values.min()])))

        df = df.append(
            pd.DataFrame(
                [[
                    x_strat_id,
                    y_strat_id,
                    *(
                        C[:, y_strat_id]
                        + df.loc[
                            df.index[-1], x_strat_names[0]:x_strat_names[-1]
                        ]
                    ),
                    *(
                        C[x_strat_id, :]
                        + df.loc[
                            df.index[-1], y_strat_names[0]:y_strat_names[-1]
                        ]
                    )
                ]],
                columns=['A', 'B', *x_strat_names, *y_strat_names],
            ),
            ignore_index=True
        )

        df.loc[df.index[-1], 'high'] = df.loc[
            df.index[-1],
            x_strat_names[0]: x_strat_names[-1]
        ].max() / df.shape[0]

        df.loc[df.index[-1], 'low'] = df.loc[
            df.index[-1], y_strat_names[0]: y_strat_names[-1]
        ].min() / df.shape[0]

        df.loc[df.index[-1], 'e'] = df['high'].min() - df['low'].max()

    x = get_counts(df['A'], range(C.shape[0]))
    y = get_counts(df['B'], range(C.shape[1]))

    return x / df.shape[0], y / df.shape[0]


def analytical_solution(x, y, H):
    x_0 = sym.solvers.solve(sym.diff(H, x, 1), x)[0]
    y_0 = sym.solvers.solve(sym.diff(H, y, 1), y)[0]
    res = sym.solvers.solve([x_0.subs(y, y_0) - x, y_0.subs(x, x_0) - y])

    return res, H.subs(res)


def saddle_point(mat):
    i_vec = np.amin(mat, 1)
    j_vec = np.amax(mat, 0)

    if i_vec.max() == j_vec.min():
        i = i_vec.argmax()
        j = j_vec.argmin()

        return i, j

    return None


def numerical_solution(x, y, H):
    def create_matrix(N: int):
        sp = np.linspace(0, 1, num=N+1)

        return np.array([
            [H.subs(x, x_).subs(y, y_) for y_ in sp]
            for x_ in sp
        ])

    N = 2
    Hmat = create_matrix(N)
    print(
        'N={}\n{}'.format(
            N,
            np.array_str(
                Hmat.astype(float), precision=3, max_line_width=80
            )
        )
    )

    s = saddle_point(Hmat)
    if s is None:
        s = [np.argmax(_) for _ in braun_robinson(Hmat)]
        print('Седловой точки нет, решение методом Брауна-Робинсон:')
    else:
        print('Есть седловая точка:')

    res = s[0] / (N + 1), s[1] / (N + 1), Hmat[tuple(s)]
    print_result(*res)

    while True:
        N += 1

        Hmat = create_matrix(N)
        print(
            'N={}\n{}'.format(
                N,
                np.array_str(
                    Hmat.astype(float), precision=3, max_line_width=80
                )
            )
        )

        s = saddle_point(Hmat)
        if s is None:
            s = [np.argmax(_) for _ in braun_robinson(Hmat)]
            print('Седловой точки нет, решение методом Брауна-Робинсон:')
        else:
            print('Есть седловая точка:')

        _res = s[0] / (N + 1), s[1] / (N + 1), Hmat[tuple(s)]
        print_result(*res)
        print()

        if abs(res[2] - _res[2]) <= 0.01:
            return _res

        res = _res


def main():
    a = eval(input('a = '))
    b = eval(input('b = '))
    c = eval(input('c = '))
    d = eval(input('d = '))
    e = eval(input('e = '))

    x = sym.Symbol('x')
    y = sym.Symbol('y')
    H = (a * (x ** 2)) + (b * (y ** 2)) + (c * x * y) + (d * x) + (e * y)

    assert sym.diff(H, x, 2) < 0, 'Hxx >= 0'
    assert sym.diff(H, y, 2) > 0, 'Hyy <= 0'

    sa = analytical_solution(x, y, H)
    sa = sa[0][x], sa[0][y], sa[1]
    print('Аналитическое решение:')
    print_result(*sa)
    print()

    print('Численное решение:')
    sn = numerical_solution(x, y, H)

    print('Погрешности:')
    print(
        *['{}={}%'.format(n, str(round(abs((sa[i] - sn[i]) / sa[i]) * 100, 1)))
            for i, n in zip(range(3), ['x', 'y', 'H'])]
    )


if __name__ == "__main__":
    main()
