#!/usr/bin/env python3
from typing import Tuple

import numpy as np


def is_nash_eq(C: np.ndarray) -> np.ndarray:
    '''
    Returns Nash equilibrium set.

    Parameters
    ==========
    C
        Game bimatrix.

    Returns
    =======
    numpy.ndarray
        Boolean matrix where True is elements of Nash equilibrium set.
    '''
    def candidates(mat):
        return (np.amax(mat, 0).T == mat)

    A, B = C[:, :, 0], C[:, :, 1].T

    return candidates(B).T & candidates(A)


def is_pareto_opt(C: np.ndarray) -> np.ndarray:
    '''
    Returns Pareto optimality set.

    Parameters
    ==========
    C
        Game bimatrix.

    Returns
    =======
    numpy.ndarray
        Boolean matrix where True is elements of Pareto optimality set.
    '''
    candidates = list()
    for idx in np.ndindex(*C.shape[:2]):
        target = C[idx]

        column = C[:, idx[1]]
        has_better_in_column = np.any(
            (target[0] < column[:, 0]) & (target[1] <= column[:, 1])
        )

        row = C[idx[0], :]
        has_better_in_row = np.any(
            (target[1] < row[:, 1]) & (target[0] <= row[:, 0])
        )

        if not (has_better_in_column | has_better_in_row):
            candidates.append(idx)

    candidates = np.array(candidates).T
    candidates_val = C[tuple(candidates)]

    optimal = candidates[:, [
        # check if there is no more profit stats
        (np.logical_and.reduce((val <= candidates_val).T).sum() == 1)
        for val in candidates_val
    ]]

    result = np.full(C.shape[:2], False)
    result[tuple(optimal)] = True

    return result


def check_for_dominant_strategy(mat: np.ndarray) -> bool:
    '''
    Check if player has strictly dominant stategy.

    Parameters
    ==========
    mat
        value matrix for player.

    Returns
    =======
    bool
        True if player has strictly dominant strategy.
    '''
    def is_row_dominant(row):
        return np.all(  # `True` if `row` is the greates
            # check if `row` is greater or equal than others
            np.apply_along_axis(
                np.all, 0,
                # there is only one equal row
                # because `mat` should be non-degenerate
                (row <= mat)
            )
        )

    return np.any(np.apply_along_axis(is_row_dominant, 1, mat))


def mixed_strategy(C: np.ndarray) -> Tuple[
    np.array, np.array, Tuple[float, float]
]:
    '''
    Get mixed strategies for bimatrix game.

    Parameters
    ==========
    C
        Game bimatrix.

    Returns
    =======
    np.array
        First player win.

    np.array
        Second player win.

    Tuple[float, float]
        Players wind.
    '''

    A, B = C[:, :, 0], C[:, :, 1]

    u = np.ones(A.shape[0])
    def v(mat): return 1 / np.dot((np.dot(u, np.linalg.inv(mat))), u)

    v1 = v(A)
    v2 = v(B)

    x = np.dot(np.dot(v2, u), np.linalg.inv(B))
    y = np.dot(np.dot(v1, np.linalg.inv(A)), u)

    return (x, v1), (y, v2)


def print_result(indexes, mat):
    for x, y in zip(*np.nonzero(indexes)):
        print('x={} y={} v={}'.format(x, y, mat[x, y]))


def _game_part1(C: np.ndarray) -> None:
    nash = is_nash_eq(C)
    print('Равновесие по Нэшу:')
    print_result(nash, C)

    pareto = is_pareto_opt(C)
    print('Оптимальность по Парето')
    print_result(pareto, C)

    print('Пересечение:')
    print_result(nash & pareto, C)


def _game_part2(C):
    A, B = C[:, :, 0], C[:, :, 1]

    clean = is_nash_eq(C)

    if check_for_dominant_strategy(A) or check_for_dominant_strategy(B):
        assert clean.sum() == 1, 'Theorem L5.1 not working.'
        print('Существует доминантная стратегия, равновесие по Нэшу:')
        print_result(clean, C)

        return

    if clean.sum() == 0:
        print('Игра не имеет ситуации равновесия по Нэшу в чистых стратегиях.')
        print('Вполне смешанная ситуация равновесия:', mixed_strategy(C))

        return

    if clean.sum() == 2:
        print('Игра имеет два ситуации равновесия по Нэшу в чистых стратегиях:')
        print_result(clean, C)
        print('Смешанная ситуация равновесия:')
        print('x={} y={} v={}'.format(*mixed_strategy(C)))

        return


def main():
    # Lab part 1
    eps = float(input('Введите e для игры "Перекресток": '))
    cross = np.array([
        [(1, 1), (1-eps, 2)],
        [(2, 1-eps), (0, 0)]
    ])
    fam = np.array([
        [(4, 1), (0, 0)],
        [(0, 0), (1, 4)]
    ])
    prison = np.array([
        [(-5, -5), (0, -10)],
        [(-10, 0), (-1, -1)]
    ])
    C = np.stack(
        [np.random.randint(-50, high=50, size=(10, 10)) for _ in range(2)],
        axis=-1
    )

    for name, game in zip(
        ('Случайная матрица', 'Семейный спор',
         'Перекресток', 'Дилемма заключенного'),
        (C, fam, cross, prison)
    ):
        print(name)
        print(np.transpose(game, (2, 0, 1)))
        _game_part1(game)
        print()

    # Lab part 2
    C = np.array([
        [(3, 1), (5, 0)],
        [(9, 6), (2, 3)]
    ])
    print('Матрица по варианту:')
    print(np.transpose(C, (2, 0, 1)))
    _game_part2(C)


if __name__ == '__main__':
    main()
